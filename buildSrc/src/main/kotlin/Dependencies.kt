object Versions {
    const val code = 1
    const val versionName = "1.0"
    const val minSdk = 21
    const val targetSdk = 29
    const val compileSdk = 29
    const val buildTools = "30.0.3"
    const val kotlin = "1.5.30"
    const val coreKtx = "1.6.0"
    const val junitAndroid = "1.1.3"
    const val junit = "4.+"
    const val runner = "1.2.0"
    const val espressoCore = "3.4.0"
    const val appCompat = "1.3.1"
    const val android = "3.5.0"
    const val material = "1.4.0"
    const val constraintLayout = "2.1.1"
    const val retrofit = "2.9.0"
    const val hilt = "2.38.1"
    const val coroutine = "1.3.9"
    const val viewModelScope = "2.2.0"
    const val navigation = "2.3.5"
    const val swipeRefreshLayout = "1.1.0"
}

object Deps {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val junitAndroid = "androidx.test.ext:junit:${Versions.junitAndroid}"
    const val junit = "junit:junit:${Versions.junit}"
    const val runner = "androidx.test:runner:${Versions.runner}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
    const val hiltCompiler = "com.google.dagger:hilt-compiler:${Versions.hilt}"
    const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutine}"
    const val viewModelScope = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.viewModelScope}"
    const val navigation = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefreshLayout}"
}

object Plugins {
    const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val androidPlugin = "com.android.tools.build:gradle:${Versions.android}"
    const val hiltPlugin = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"
    const val safeArgsPlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
}