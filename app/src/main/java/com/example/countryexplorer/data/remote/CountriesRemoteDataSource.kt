package com.example.countryexplorer.data.remote

import com.example.countryexplorer.data.getResult
import com.example.countryexplorer.data.service.CountriesApi
import com.example.countryexplorer.data.toCountryInfo
import com.example.countryexplorer.domain.CountryInfo
import com.example.countryexplorer.domain.datasource.CountriesDataSource
import javax.inject.Inject

class CountriesRemoteDataSource @Inject constructor(private val api: CountriesApi) : CountriesDataSource {

    override suspend fun getCountries(): List<CountryInfo> {
        return getResult { api.getCountries() }.map { it.toCountryInfo() }

    }
}