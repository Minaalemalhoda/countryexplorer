package com.example.countryexplorer.data

import com.example.countryexplorer.domain.CountryInfo

fun CountryInfoDto.toCountryInfo() = CountryInfo(name, region, capital, callingCodes, flagIconName)