package com.example.countryexplorer.data

import com.squareup.moshi.Json

data class CountryInfoDto(
    @field:Json(name = "name") val name: String,
    @field:Json(name = "region") val region: String,
    @field:Json(name = "capital") val capital: String,
    @field:Json(name = "callingCodes") val callingCodes: List<String>,
    @field:Json(name = "alpha2Code") val flagIconName: String
)