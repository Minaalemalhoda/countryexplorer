package com.example.countryexplorer.data.repository

import com.example.countryexplorer.data.remote.CountriesRemoteDataSource
import com.example.countryexplorer.domain.CountryInfo
import javax.inject.Inject

class CountriesRepository @Inject constructor(private val remoteDataSource: CountriesRemoteDataSource) {

    suspend fun getCountries(): List<CountryInfo> {
        return remoteDataSource.getCountries()
    }

}