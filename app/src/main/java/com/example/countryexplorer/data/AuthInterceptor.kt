package com.example.countryexplorer.data

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        val url =
            req.url().newBuilder()
                .addQueryParameter("access_key", ACCESS_KEY)
                .build()
        req = req.newBuilder().url(url).build()
        return chain.proceed(req)
    }

    companion object {
        const val ACCESS_KEY = "e916b37c5c17e0bb53548d74ac1418a2"
    }
}