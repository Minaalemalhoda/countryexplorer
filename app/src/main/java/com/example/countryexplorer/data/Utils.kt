package com.example.countryexplorer.data

import retrofit2.Response

suspend fun <T> getResult(call: suspend () -> Response<T>): T {
    val response = call()
    if (response.isSuccessful) {
        val body = response.body()
        if (body != null) return body
    }
    throw Exception(response.message())
}
