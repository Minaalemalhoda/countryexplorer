package com.example.countryexplorer.data.service

import com.example.countryexplorer.data.CountryInfoDto
import retrofit2.Response
import retrofit2.http.GET

interface CountriesApi {

    @GET("all")
    suspend fun getCountries(): Response<List<CountryInfoDto>>
}