package com.example.countryexplorer.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.countryexplorer.data.repository.CountriesRepository
import com.example.countryexplorer.domain.CountryInfo
import com.example.countryexplorer.domain.LoadableData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CountriesViewModel @Inject constructor(private val repository: CountriesRepository) :
    BaseViewModel() {

    private val _state: MutableLiveData<LoadableData<List<CountryInfo>>> by lazy {
        MutableLiveData(LoadableData.NotInitialized)
    }
    val state: LiveData<LoadableData<List<CountryInfo>>> = _state

    init {
        fetchCountries()
    }

    fun fetchCountries() {
        if (_state.value == LoadableData.Loading)
            return

        viewModelScope.launch {
            _state.value = LoadableData.Loading
            onBg {
                runCatching {
                    repository.getCountries()
                }
            }.fold({ value ->
                _state.value = LoadableData.Loaded(value)
                Log.d(
                    this@CountriesViewModel.javaClass.name,
                    "The list of countries has been successfully fetched."
                )

            }, {
                _state.value = LoadableData.Failed
                Log.d(
                    this@CountriesViewModel.javaClass.name,
                    "Unable to get countries...${it.message}."
                )
            })
        }
    }
}