package com.example.countryexplorer.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.countryexplorer.R
import com.example.countryexplorer.domain.CountryInfo
import com.example.countryexplorer.domain.LoadableData
import com.example.countryexplorer.presentation.ui.adapter.CountryListAdapter
import com.example.countryexplorer.presentation.viewmodel.CountriesViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_countries.*

@AndroidEntryPoint
class CountriesFragment : Fragment() {

    private var errorSnackBar: Snackbar? = null

    lateinit var adapter: CountryListAdapter

    private val viewModel: CountriesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_countries, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpCountriesList()
        enableSwipeToRefresh()
        listenToState()
    }

    private fun listenToState() {
        viewModel.state.observe(viewLifecycleOwner, { state ->
            when (state) {
                LoadableData.Failed -> {
                    swipeRefreshLayout.isRefreshing = false
                    adapter.showRetry()
                }
                LoadableData.Loading -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        adapter.showLoading()
                }
                LoadableData.NotInitialized -> {
                    swipeRefreshLayout.isRefreshing = false
                    adapter.showRetry()
                }
                is LoadableData.Loaded -> {
                    swipeRefreshLayout.isRefreshing = false
                    adapter.submitList(state.value)
                }
            }
        })
    }

    private fun setUpCountriesList() {
        adapter = CountryListAdapter({
            navigateToCountryDetail(this)
        }, {
            viewModel.fetchCountries()
        })
        countriesList.adapter = adapter
        countriesList.layoutManager = LinearLayoutManager(context)
    }

    private fun navigateToCountryDetail(info: CountryInfo) {
        findNavController(this).navigate(CountriesFragmentDirections.actionOpenDetail(info))
    }

    private fun enableSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.fetchCountries()
        }
    }

    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    private fun showError(string: String) {
        errorSnackBar = Snackbar.make(container, string, Snackbar.LENGTH_LONG).apply {
            show()
        }
    }

    private fun hideError() {
        errorSnackBar?.dismiss()
    }

    override fun onDestroy() {
        hideError()
        super.onDestroy()
    }

}