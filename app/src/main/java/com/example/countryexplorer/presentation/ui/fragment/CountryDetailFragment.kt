package com.example.countryexplorer.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.countryexplorer.R
import com.example.countryexplorer.presentation.getFlagIcon
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_country_detail.*

class CountryDetailFragment : BottomSheetDialogFragment() {

    private val args: CountryDetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_country_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        args.country.apply {
            countryCapital.text = getString(R.string.country_capital, capital)
            countryName.text = args.country.name
            countryRegion.text = getString(R.string.country_region, region)
            countryFlagIcon.setImageDrawable(getFlagIcon(this@CountryDetailFragment.requireContext()))
        }

    }

}