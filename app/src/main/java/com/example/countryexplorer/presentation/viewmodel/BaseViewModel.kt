package com.example.countryexplorer.presentation.viewmodel

import androidx.lifecycle.ViewModel
import com.example.countryexplorer.presentation.CoroutineUtility
import kotlinx.coroutines.withContext

open class BaseViewModel : ViewModel() {

    val coroutineDispatcherProvider = CoroutineUtility.coroutineDispatcherProvider()

    suspend inline fun <T> onUI(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineDispatcherProvider.uiDispatcher()) {
            coroutine()
        }
    }

    suspend inline fun <T> onBg(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineDispatcherProvider.bgDispatcher()) {
            coroutine()
        }
    }

    suspend inline fun <T> onIO(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineDispatcherProvider.ioDispatcher()) {
            coroutine()
        }
    }
}