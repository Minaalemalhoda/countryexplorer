package com.example.countryexplorer.presentation.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.countryexplorer.domain.CountryInfo
import com.example.countryexplorer.presentation.getFlagIcon
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_country_detail.*
import kotlinx.android.synthetic.main.item_country.view.*

sealed class CountryListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
    LayoutContainer {

    class CountryViewHolder(itemView: View, private val onCountryClicked: CountryInfo.() -> Unit) :
        CountryListViewHolder(itemView) {

        override val containerView: View
            get() = itemView

        fun bindView(countryInfo: CountryInfo) {
            containerView.apply {
                setOnClickListener {
                    onCountryClicked.invoke(countryInfo)
                }
                countryName.text = countryInfo.name
                countryFlagIcon.setImageDrawable(countryInfo.getFlagIcon(context))
            }
        }
    }

    class RetryViewHolder(itemView: View, private val onRetryButtonClicked: () -> Unit) :
        CountryListViewHolder(itemView) {

        override val containerView: View
            get() = itemView

        fun bind() {
            containerView.setOnClickListener {
                onRetryButtonClicked.invoke()
            }
        }
    }

    class LoadingViewHolder(itemView: View) : CountryListViewHolder(itemView) {

        override val containerView: View
            get() = itemView
    }

}