package com.example.countryexplorer.presentation.di

import com.example.countryexplorer.data.service.CountriesApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CountriesModule {

    @Provides
    @Singleton
    fun providesCountriesApi(retrofit: Retrofit) = retrofit.create(CountriesApi::class.java)
}