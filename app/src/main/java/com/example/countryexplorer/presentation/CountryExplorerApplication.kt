package com.example.countryexplorer.presentation

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CountryExplorerApplication : Application() {
}