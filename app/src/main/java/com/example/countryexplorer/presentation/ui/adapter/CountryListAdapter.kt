package com.example.countryexplorer.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.ListAdapter
import com.example.countryexplorer.R
import com.example.countryexplorer.domain.CountryInfo

class CountryListAdapter(
    private val onCountryClicked: CountryInfo.() -> Unit,
    private val onRetryButtonClicked: () -> Unit
) : ListAdapter<CountryInfo, CountryListViewHolder>(CountryInfoCallbackDiff) {

    private var isShowingLoading = true
    private var isShowingRetry = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryListViewHolder {
        return when (viewType) {
            CountryItemType.LOADING.ordinal -> CountryListViewHolder.LoadingViewHolder(
                getView(
                    R.layout.item_loading,
                    parent
                )
            )
            CountryItemType.RETRY.ordinal -> CountryListViewHolder.RetryViewHolder(
                getView(
                    R.layout.item_retry,
                    parent
                ), onRetryButtonClicked
            )
            else -> CountryListViewHolder.CountryViewHolder(
                getView(R.layout.item_country, parent),
                onCountryClicked
            )
        }
    }

    override fun onBindViewHolder(holder: CountryListViewHolder, position: Int) {
        if (holder is CountryListViewHolder.CountryViewHolder) {
            holder.bindView(getItem(position))
        } else if (holder is CountryListViewHolder.RetryViewHolder) {
            holder.bind()
        }
    }

    override fun getItemCount() =
        (if (isShowingLoading || isShowingRetry) 1 else 0) + super.getItemCount()

    override fun getItemViewType(position: Int): Int {
        if (position == itemCount - 1)
            if (isShowingLoading)
                return CountryItemType.LOADING.ordinal
            else if (isShowingRetry)
                return CountryItemType.RETRY.ordinal

        return CountryItemType.DEFAULT.ordinal
    }

    private fun getView(@LayoutRes layout: Int, parent: ViewGroup) =
        LayoutInflater.from(parent.context).inflate(
            layout,
            parent,
            false
        )

    override fun submitList(list: List<CountryInfo>?) {
        hideLoading()
        hideRetry()
        super.submitList(list)
    }

    fun showLoading() {
        if (isShowingLoading)
            return
        hideRetry()
        isShowingLoading = true
        notifyItemInserted(itemCount - 1)
    }

    private fun hideLoading() {
        if (isShowingLoading) {
            isShowingLoading = false
            notifyItemRemoved(itemCount)
        }
    }

    fun showRetry() {
        if (isShowingRetry)
            return
        hideLoading()
        isShowingRetry = true
        notifyItemInserted(itemCount - 1)
    }

    private fun hideRetry() {
        if (isShowingRetry) {
            isShowingRetry = false
            notifyItemRemoved(itemCount)
        }
    }

    enum class CountryItemType {
        RETRY,
        LOADING,
        DEFAULT
    }

}
