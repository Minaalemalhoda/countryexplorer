package com.example.countryexplorer.presentation.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.countryexplorer.domain.CountryInfo

object CountryInfoCallbackDiff : DiffUtil.ItemCallback<CountryInfo>() {

    override fun areItemsTheSame(oldItem: CountryInfo, newItem: CountryInfo): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: CountryInfo, newItem: CountryInfo): Boolean {
        return newItem == oldItem
    }
}