package com.example.countryexplorer.presentation

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.content.res.AppCompatResources
import com.example.countryexplorer.domain.CountryInfo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

object CoroutineUtility {

    @JvmStatic
    fun coroutineDispatcherProvider() = object : CoroutineDispatcherProvider {
        override fun bgDispatcher(): CoroutineDispatcher {
            return Dispatchers.Default
        }

        override fun ioDispatcher(): CoroutineDispatcher {
            return Dispatchers.IO
        }

        override fun uiDispatcher(): CoroutineDispatcher {
            return Dispatchers.Main
        }

    }

}

interface CoroutineDispatcherProvider {

    fun bgDispatcher(): CoroutineDispatcher
    fun uiDispatcher(): CoroutineDispatcher
    fun ioDispatcher(): CoroutineDispatcher
}

fun CountryInfo.getFlagIcon(context: Context): Drawable? {
    var id = context.getDrawableId(flagIconName.toLowerCase()).takeIf { it > 0 } ?: run {
        context.getDrawableId("${flagIconName.toLowerCase()}_flag")
    }
    if (id == 0) id = context.getDrawableId("placeholder")
    return AppCompatResources.getDrawable(
        context,
        id
    )
}

fun Context.getDrawableId(name: String) = resources.getIdentifier(
    name, "drawable",
    this.packageName
)