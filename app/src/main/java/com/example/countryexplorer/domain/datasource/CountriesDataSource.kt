package com.example.countryexplorer.domain.datasource

import com.example.countryexplorer.domain.CountryInfo

interface CountriesDataSource {
    suspend fun getCountries(): List<CountryInfo>
}