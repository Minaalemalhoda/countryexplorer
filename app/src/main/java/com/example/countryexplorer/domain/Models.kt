package com.example.countryexplorer.domain

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

sealed class LoadableData<out T> {
    object Loading : LoadableData<Nothing>()
    class Loaded<T>(val value: T) : LoadableData<T>()
    object Failed : LoadableData<Nothing>()
    object NotInitialized : LoadableData<Nothing>()
}

@Parcelize
@Keep
class CountryInfo(
    val name: String,
    val region: String,
    val capital: String,
    val callingCodes: List<String>,
    val flagIconName: String
) : Parcelable